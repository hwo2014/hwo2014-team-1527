﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Piece {
    // For a Straight the length attribute depicts the length of the piece.
    // Additionally, there may be a switch attribute that indicates that this piece is a switch piece, i.e. you can switch lanes here.
    // The bridge attribute indicates that this piece is on a bridge.
    //   This attribute is merely a visualization cue and does not affect the actual game.
    float length = -1.0f;
    bool @switch = false;

    // For a Bend, there's radius that depicts the radius of the circle that would be formed by this kind of pieces.
    //   More specifically, this is the radius at the center line of the track.
    //   The angle attribute is the angle of the circular segment that the piece forms.
    // For left turns, this is a negative value.
    //   For a 45-degree bend to the left, the angle would be -45.
    //   For a 30-degree turn to the right, this would be 30.
    int radius = -1;
    float angle = -1.0f;
}

// The track may have 1 to 4 lanes.
// The lanes are described in the protocol as an array of objects that indicate
//   the lane's distance from the track center line.
// A positive value tells that the lanes is to the right from the center line
//   while a negative value indicates a lane to the left from the center.
public class Lane {
    int distanceFromCenter = 0;
    int index = 0;
}