﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// see TurboAvailable : ServerMessage
public class Turbo {
    public float turboDurationMilliseconds;
    public int turboDurationTicks;
    public float turboFactor;
}

// see CarPositions : ServerMessage
public class Point2D {
    public float x;
    public float y;
}

// see CarPositions : ServerMessage
public class Frame2D {
    public Point2D position;
    public float angle;
}

// The cars on the track are described by their id (color, bot name) and dimensions.
public class Car : Frame2D {
    // see Join : ServerMessage
    // see YourCar : ServerMessage
    // Allows the bot to identify its own car on the track.
    //   Basically it's the *color* that matters. (i.e. map cars by color!)
    public class Id {
        public string name;
        public string color;
        public string key;
    }

    // see GameInit : ServerMessage
    // The dimensions contain car width and lenght as well as the position of the Guide Flag.
    // The guide flag is the part that keeps the car on the track.
    public Id id;
    public Geometry dimensions;
    public class Geometry {
        public float width;
        public float height;
        public float guideFlagPosition;
    }

    // see CarPositions : ServerMessage
    // The angle depicts the car's slip angle. 
    //   Normally this is zero, but when you go to a bend fast enough, the car's tail will start to drift. 
    //   Naturally, there are limits to how much you can drift without crashing out of the track.
    public Position piecePosition;
    public class Position {
        public int pieceIndex = -1;             // zero based index of the piece the car is on
        public float inPieceDistance = -1.0f;   //  the distance the car's Guide Flag (see above) has traveled from the start of the piece along the current lane
        public LanePlan lane;       // Usually startLaneIndex and endLaneIndex are equal, but they do differ when the car is currently switching lane
        public class LanePlan {
            public int startLaneIndex;
            public int endLaneIndex;
        }
        // The number of laps the car has completed.
        // The number 0 indicates that the car is on its first lap.
        // The number -1 indicates that it has not yet crossed the start line
        //   to begin it's[sic] first lap. (srsl hwo documenters...)
        public int lap = -2;
    }

    // see GameEnd : ServerMessage
    public Id car { get { return id; } set { id = value; } }  // alias with id, they are the same thing
    //public Id car;
    public Result result;       // used for three things: best laps, final results, and intermediate results after lap finished
    public class Result {
        public int lap = -1;    // for bestLaps (lap number of the best lap for this car)
        public int laps = -1;   // for results (total number of laps by this car)
        public int ticks;       // timestamp of the finish / best lap
        public int millis;      // timestamp in milliseconds  
    }

    // see LapFinished : ServerMessage
    public Result lapTime;
    public Result raceTime;
    public Ranking ranking;
    public class Ranking {
        public int overall;
        public int fastestLap;
    }

    // see Disqualified : ServerMessage
    // Reason is typically a failure to comply with the bot protocol or that
    //   the bot has disconnected.
    public string reason;       // for disqualification, obv...
}
