using System;
using System.IO;
using System.Linq;

public class Bot : Car {

    // Example derp bot implementation

    // AI main loop
    // Called every car position update
    // Override on your own Bot class
    // send() out commands to server, most importantly a throttle value
    public virtual void ExecuteCunningPlan() {
        debug("I'm at " + piecePosition.inPieceDistance + " on piece #" + piecePosition.pieceIndex, 2);

        int pi = piecePosition.pieceIndex;
        if (pi != currentPieceIndex) {
            log("Moved to piece #" + pi);
            currentPieceIndex = pi;
        }
        send(new Throttle(0.5));
    }
    int currentPieceIndex = -1;

    // BELOW THIS IS BOILERPLATE THAT *SHOULD*HAVE* BEEN THERE ALREADY! Goddamnit...

    protected Race race;
    protected Turbo currentTurbo = null;

    protected int tick = 0;
    protected string gameId;

    public Bot(string botName, string botKey) {
        id = new Car.Id();
        id.name = botName;
        id.key = botKey;
        id.color = "RaindropsOnFngRoses";
    }

    public void React(ServerMessage msg) {
        if (msg is JoinResponse) {
            var data = ((JoinResponse)msg).data;
            debug("Joined race at " + data.trackName + ", so excitement!");
        } else if (msg is YourCar) {
            id = ((YourCar)msg).data;   // id.color is primary key for "me"
            debug(id.name + " car info received: I'm " + id.color);
        } else if (msg is GameInit) {
            race = ((GameInit)msg).data.race;
            debug("Race initialized: " + race.track.name + " " + race.raceSession.laps + " laps");
        } else if (msg is GameStart) {
            debug("Race starts!");
        } else if (msg is CarPositions) {
            tick = msg.gameTick;
            var cars = ((CarPositions)msg).data;
            var updated = cars.Where(c => c.id.color == id.color).First();  // protocol guarantees there is one and only one
            angle = updated.angle;  // drift angle, should(?) be 0
            piecePosition = updated.piecePosition;            
            debug("Car positions updated", 2);
            ExecuteCunningPlan();
            debug("AI step done, replies sent", 2);
        } else if (msg is TurboAvailable) {
            currentTurbo = ((TurboAvailable)msg).data;
            debug("Got sweet turbo x" + currentTurbo.turboFactor + ", now what?");
        } else if (msg is Spawn) {
            var spawner = ((Spawn)msg).data;
            debug(spawner.name + " respawned, hallelujah!");
        } else if (msg is LapFinished) {
            var finisher = ((LapFinished)msg).data;
            debug(finisher.car.name + " completed a lap! Much rally!");
            if (finisher.car.color == car.color) {
                debug("It was me! Woo! I'm number " + finisher.ranking.overall);
            }            
        } else if (msg is Disqualified) {
            var dnf = ((Disqualified)msg).data;
            debug(dnf.id.name + " got b&, reason: " + dnf.reason);
            if (dnf.id.color == id.color) {
                debug("Oh no! That was me! This sucks! Now what do?");    // invoke hack?
            }
        } else if (msg is GameEnd) {
            var laps = ((GameEnd)msg).data.bestLaps;
            var myBest = laps
                .Where(lapInfo => lapInfo.id != null && lapInfo.id.color == id.color)
                .Select(lapInfo => lapInfo.result)
                .FirstOrDefault();
            if (myBest != null) {
                debug("Race ended, best lap number " + myBest.lap + " (" + myBest.millis + "ms, " + myBest.ticks + " ticks)");
            }
        } else if (msg is TournamentEnd) {            
            debug("Tournament ended, expecting server to cut the connection soon...");
        } else if (msg is Crash) {
            var crasher = ((Crash)msg).data;
            debug(crasher.name + " crashed, such noob, wow.");
            if (crasher.color == id.color) {
                debug("Oh no! That was me! This sucks! Waiting for N seconds then I guess...");
            }
        } else if (msg is Finish) {
            var finisher = ((Finish)msg).data;
            debug(finisher.name + " got past the finish line! Grats!");
            if (finisher.color == id.color) {
                debug("Yay! It was me! Party time!");
            }
        } else {
            debug("Unhandled message: " + msg.msgType);
        }

        if (msg.gameId != null) { gameId = msg.gameId; }
        if (msg.gameTick > 0) { tick = msg.gameTick; }
        send(new Ping());   // DDoS the f*er
    }

    public static StreamWriter writer = null;

	public static void send(BotMessage msg) {
        if (writer == null) return;
        writer.WriteLine(msg.ToJson());
	}

    // filter out too much verbosity
    public static int debugLevel = 0;
    public static void debug(string msg, int priority = 1) {
        if (priority <= debugLevel) {
            log(msg, "DEBUG");
        }
        System.Diagnostics.Debug.WriteLine(msg);
    }

    public static void log(string msg, string source = "BOT") {
        string msgText = source + ": " + msg;
        Console.WriteLine(msgText);        
    }
}
