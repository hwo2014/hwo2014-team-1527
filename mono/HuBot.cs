﻿using System;
using System.Collections.Generic;
using System.Linq;

public class HuBot : Bot {
    public HuBot(string botName, string botKey) : base(botName, botKey) {}

    // AI main loop
    // Called every car position update
    // send() out commands to server, most importantly a throttle value
    public override void ExecuteCunningPlan() {

        // TODO: figure out special features

        if (currentTurbo != null) {
            // TODO: figure out how to use it
            //send(new MasTurbo());     // it's Spanish, you see.
            currentTurbo = null;
        }

        double throttle = 0.5;

        // TODO: do something smart here

        send(new Throttle(throttle));
    }
}