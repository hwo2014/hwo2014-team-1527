﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

public class ChihuoRocks
{
    public static string[] tracks = {
        "keimola",
        "germany",
        "usa",
        "hockenheim",
        "indianapolis"
    };

    /**
    * Entry point
    */ 
    public static void Main(string[] args)
    {        
        // parse command-line (used by CI server!)
        string host = args.Length > 0 ? args[0] : "testserver.helloworldopen.com";
        int port = args.Length > 1 ? int.Parse(args[1]) : 8091;
        string botName = args.Length > 2 ? args[2] : "CHIHUO";
        string botKey = args.Length > 3 ? args[3] : "WgfHdhBI/LRdeg";        

        if (args.Length > 0 && args[0] == "test") {
            TestJSON();
            TestServerMessages();
            Console.ReadLine();
        } else {
            Bot.debugLevel = 1;     // 0 = only bot messages, 1 = debug, 2 = verbose, 3 = s00per t4lkativ3!
            var bot = new Bot(botName, botKey);
            //var bot = new HuBot(botName, botKey);
            Run(host, port, bot, rand.Next(0, 3));
        }
    }

    /**
    * Main loop
    */
    public static void Run(string host, int port, Bot bot, int trackNumber) {
        Console.WriteLine("Connecting to " + host + ":" + port + " as " + bot.id.name + "/" + bot.id.key);

        using (TcpClient client = new TcpClient(host, port)) {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            Bot.writer = new StreamWriter(stream);
            Bot.writer.AutoFlush = true;

            var track = tracks[trackNumber];

            Bot.send(new Join(bot.id.name, bot.id.key, track));

            string line;
            while ((line = reader.ReadLine()) != null) {
                var msg = ServerMessage.Parse(line);
                bot.React(msg);
            }
        }

        Console.WriteLine("Disconnected. Press AnyKey to continue...");
        Console.ReadLine();
        Console.WriteLine("No, not that one! NOOOO!");
    }

    public static Random rand = new Random();

    /**
    * Run through server message parsing for most important server messages
    * Samples collected from the docs
    */
    public static void TestServerMessages() {
        Console.WriteLine("Testing server message parsing... ");
        ServerMessage msg;

        string yourCarJSON =
            "{\"msgType\": \"yourCar\", \"data\": {" +
                "\"name\": \"Schumacher\"," +
                "\"color\": \"red\"" +
            "}}";            
        msg = ServerMessage.Parse(yourCarJSON);
        if (msg is YourCar) {                
            var car = ((YourCar)msg).data;
            Console.WriteLine("Correct type (YourCar), name: " + car.name);                
        } else {
            Console.WriteLine("Wrong type");
        }

        string gameInitJSON = System.IO.File
            .ReadAllText("SampleMessages\\usaGameInitMessage.txt")
            .Replace("\n", "")
            .Replace("\r", "")
            .Replace("\t", "")
            .Replace(" ", "");
        msg = ServerMessage.Parse(gameInitJSON);
        if (msg == null) {
            Console.WriteLine("Got null msg!");
        } else if (msg is GameInit) {
            var race = ((GameInit)msg).data.race;
            if (race == null) {
                Console.WriteLine("Got null race!");
            } else if (race.track == null) {
                Console.WriteLine("Got null race track!");
            } else {
                Console.WriteLine("Successfully parsed GameInit, track ID: " + race.track.id);
                Console.WriteLine("Cars: " + string.Join(", ", race.cars.Select(c => c.id.color)));
                Console.WriteLine("Laps: " + race.raceSession.laps + ", timeLimit(ms): " + race.raceSession.maxLapTimeMs);
            }
        } else {
            Console.WriteLine("Wrong type");
        }

        string carPosJSON = System.IO.File
            .ReadAllText("SampleMessages\\sampleCarPositionsMessage.txt")
            .Replace("\n", "")
            .Replace("\r", "")
            .Replace("\t", "")
            .Replace(" ", "");
        msg = ServerMessage.Parse(carPosJSON);
        if (msg == null) {
            Console.WriteLine("Got null msg!");
        } else if (msg is CarPositions) {
            var pos = ((CarPositions)msg).data;
            if (pos == null) {
                Console.WriteLine("Got null pos!");
            } else if (pos.Count < 1) {
                Console.WriteLine("Got empty pos!");
            } else {
                Console.WriteLine("Successfully parsed CarPositions, number of cars: " + pos.Count);
                foreach (var car in pos.Select(p => new { p.id.name, p.id.color, p.piecePosition.lane.startLaneIndex })) {
                    Console.WriteLine(car.name + " (" + car.color + " car) on lane " + car.startLaneIndex);
                }
            }
        } else {
            Console.WriteLine("Wrong type");
        }
    }

    public static void TestJSON() {
        string json = "{'msgType':'moi','data':{'race':{'track':{'id':'moi'}}}}";
        //string json2 = "{'msgType':'moi','data':{'track':{'id':'moi'}}}";
        var msg = JsonConvert.DeserializeObject<GameInit>(json);
        if (msg == null || msg.data == null || msg.data.race == null ||
                msg.data.race.track == null) {
            Console.WriteLine("Parsing failed, probably");
        } else if (msg.data.race.track.id == "moi") {
            Console.WriteLine("Parsing successful");
        } else {
            Console.WriteLine("Parsing failed, corrupted");
        }            
    }
}
