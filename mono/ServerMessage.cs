﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

// Maximum length for bot name is 16 characters.
// The key is the Bot Key that you can find on your team page or 
//   in the config file in your repository. (EDIT: or in Program.cs)
// The server will acknowledge this by replying with the same message (as Join : BotMessage)
public class JoinResponse : ServerMessage {
    public Join data;
}
public class OldJoinResponse : ServerMessage {
    public Car.Id data;
}

// This message allows the bot to identify its own car on the track. Basically it's the color that matters.
public class YourCar : ServerMessage {
    public Car.Id data;
}

// The gameInit message describes the racing track, the current racing session
//   and the cars on track.
// The track is described as an array of pieces that may be either Straights or Bends.
public class GameInit : ServerMessage {
    public RaceData data;
    public class RaceData {     // overwhelming racism here, why so nested?
        public Race race;
    }
}

// The race has started!
public class GameStart : ServerMessage {
    public string data;  // null
}

// The carPositions message describes the position of each car on the track.
public class CarPositions : ServerMessage {
    public List<Car> data;
}

// When the game is over, the server sends the gameEnd messages that contains race results.
public class GameEnd : ServerMessage {
    public Race data;
}

// No documentation! Whoopee! Maybe we should launch hack?
public class TournamentEnd : ServerMessage {
    public string data;  // null
}

// Server sends crash message when a car gets out of track.
public class Crash : ServerMessage {
    public Car.Id data;
}

// Shortly after the crash, server sends spawn message as the car is restored on the track.
// When you crash, you'll be off the track for N seconds (typically 5).
public class Spawn : ServerMessage {
    public Car.Id data;
}

// Server sends lapFinished message when _a_car_ completes a lap. (for monitoring all players...)
public class LapFinished : ServerMessage {
    public Car data;
}

// Server sends dnf message when a car is disqualified.
// Reason is typically a failure to comply with the bot protocol or that
//   the bot has disconnected.
public class Disqualified : ServerMessage {
    public Car data;
}

// Server sends finish message when _a_car_ finishes the race. (for monitoring all players...)
public class Finish : ServerMessage {
    public Car.Id data;
}

public class TurboAvailable : ServerMessage {
    public Turbo data;
}

public class ServerMessage {
    public string msgType = null;
    public string gameId = null;
    public int gameTick = -1;   // for sync with server, each update is gameTick++

    public static ServerMessage Parse(string msgJSON) {
        Bot.debug("ServerMessage: " + msgJSON, 3);

        // Maybe parse the msgType with regex or something,
        //   to avoid re-parsing it into the final specialized ServerMessage class
        // Who cares about perf anyway.
        ServerMessage msg = JsonConvert.DeserializeObject<ServerMessage>(msgJSON);
        switch (msg.msgType) {
            case "join": msg = JsonConvert.DeserializeObject<OldJoinResponse>(msgJSON); break;
            case "joinRace": msg = JsonConvert.DeserializeObject<JoinResponse>(msgJSON); break;
            case "yourCar": msg = JsonConvert.DeserializeObject<YourCar>(msgJSON); break;
            case "gameInit":
                msg = JsonConvert.DeserializeObject<GameInit>(msgJSON);
                Bot.debug("gameInit message:", 2);
                Bot.debug(msgJSON, 2);
                break;
            case "gameStart": msg = JsonConvert.DeserializeObject<GameStart>(msgJSON); break;
            case "carPositions": msg = JsonConvert.DeserializeObject<CarPositions>(msgJSON); break;
            case "gameEnd": msg = JsonConvert.DeserializeObject<GameEnd>(msgJSON); break;
            case "tournamentEnd": msg = JsonConvert.DeserializeObject<TournamentEnd>(msgJSON); break;
            case "crash": msg = JsonConvert.DeserializeObject<Crash>(msgJSON); break;
            case "spawn": msg = JsonConvert.DeserializeObject<Spawn>(msgJSON); break;
            case "lapFinished": msg = JsonConvert.DeserializeObject<LapFinished>(msgJSON); break;
            case "dnf": msg = JsonConvert.DeserializeObject<Disqualified>(msgJSON); break;
            case "finish": msg = JsonConvert.DeserializeObject<Finish>(msgJSON); break;

            case "turboAvailable":
                msg = JsonConvert.DeserializeObject<TurboAvailable>(msgJSON);
                Bot.debug("turboAvailable message:", 2);
                Bot.debug(msgJSON, 2);
                break;

            default:
                Bot.debug("WARNING: ignored undocumented server message, msgType: " + msg.msgType);
                Bot.debug(msgJSON, 2);
                break;
        }

        return msg;
    }    
}