﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Race {
    // see GameInit : ServerMessage
    public Track track;
    public class Track {
        public string id;
        public string name;
        public List<Piece> pieces;
        public List<Lane> lanes;

        // The startingPoint attribute is a visualization cue and can be ignored.
        public Frame2D startingPoint;
    }
    public List<Car> cars;
    public Session raceSession;
    public class Session {
        public int laps = -1;
        public int maxLapTimeMs = -1;
        public bool quickRace = false;
    }

    // see GameEnd : ServerMessage
    public List<Car> results;
    public List<Car> bestLaps;
}
