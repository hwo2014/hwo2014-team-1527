﻿using System;

class BotMessageWrapper {
    public string msgType;
    public Object data;

    public BotMessageWrapper(string msgType, Object data) {
        this.msgType = msgType;
        this.data = data;
    }
}

public abstract class BotMessage {
    public string ToJson() {
        return Newtonsoft.Json.JsonConvert.SerializeObject(new BotMessageWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData() {
        return this;
    }
    protected abstract string MsgType();
}

public class Join : BotMessage {
    public Car.Id botId;
    public string trackName;

    public Join(string name, string key, string trackId) {
        botId = new Car.Id();
        botId.name = name;
        botId.color = "red";
        botId.key = key;
        trackName = trackId;
    }

    protected override string MsgType() {
        return "joinRace";
    }
}

public class Ping : BotMessage {
    protected override string MsgType() {
        return "ping";
    }
}

public class Throttle : BotMessage {
    public double value;

    public Throttle(double value) {
        this.value = value;
    }

    protected override Object MsgData() {
        return this.value;
    }

    protected override string MsgType() {
        return "throttle";
    }
}

public class SwitchLane : BotMessage {
    string dir;

    public SwitchLane(string direction) {
        // make sure dir value is legal...
        this.dir = (direction == "Left") ? "Left" : "Right";
    }

    protected override Object MsgData() {
        return dir;
    }

    protected override string MsgType() {
        return "switchLane";
    }
}
